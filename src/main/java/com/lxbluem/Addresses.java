package com.lxbluem;

public class Addresses {
    public static final String ROUTE_ADD = "route";
    public static final String ROUTE_REMOVE = "unroute";
    public static final String BOT_INIT = "bot.init";
    public static final String BOT_FAIL = "bot.fail";
    public static final String BOT_DCC_INIT = "bot.dcc.init";
    public static final String BOT_DCC_START = "bot.dcc.start";
    public static final String BOT_DCC_PROGRESS = "bot.dcc.progress";
    public static final String BOT_DCC_QUEUE = "bot.dcc.queue";
    public static final String BOT_DCC_FINISH = "bot.dcc.finish";
    public static final String FILENAME_RESOLVE = "filename.resolve";
    public static final String BOT_NOTICE = "bot.notice";
    public static final String BOT_UPDATE_NICK = "bot.nick";
    public static final String BOT_EXIT = "bot.exit";
    public static final String STATE = "state";
}
