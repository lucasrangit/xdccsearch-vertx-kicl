package com.lxbluem.state;

public enum DccState {
    INIT, START, PROGRESS, FINISH, FAIL;
}
